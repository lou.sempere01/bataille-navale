  protocol BateauProtocol{
    /**
    * L'identifiant du bateau.
    * id : BateauProtocol -> Int
    **/
    var id : Int { get } 
    /**
    * La taille initiale du bateau.
    * taille : BateauProtocol -> Int
    * 1 <= taille <= 4 
    **/
    var taille : Int { get }
    /**
    * Etat du bateau, coulé ou non. Vrai si le bateau est coulé.
    * estcoule : BateauProtocol -> Bool
    * nbPositionsTouche == taille
    **/
    var estcoule : Bool { get }
    /**
    * Ensembles des positions fixes/initiales du bateau.
    * positionsBat : BateauProtocol -> {(lig:Int, col:Int)}
    * lig et col sont inclues dans les emplacements possibles du jeu. 0 <= lig, col <= tailleDuJeu 
    **/
    var positionsBat : [(lig:Int, col:Int)] { get }
    /**
    * Les positions touchées du bateau.
    * positionsTouche : BateauProtocol -> {(lig:Int, col:Int)|Vide}
    * lig et col sont inclues dans les emplacements possibles du jeu. 0 <= lig, col <= tailleDuJeu 
    * un couple lig,col est une position existante dans positionsBat.
    **/
    var positionsTouche : [(lig:Int, col:Int)?] { get set }
    /**
    * Les positions restantes non touchées du bateau.
    * positionsRestantes : BateauProtocol -> {(lig:Int, col:Int)|Vide}
    * lig et col sont inclues dans les emplacements possibles du jeu. 0 <= lig, col <= tailleDuJeu 
    * un couple lig,col est une position existante dans positionsBat.
    **/
    var positionsRestantes : [(lig: Int, col: Int)?] { get set }
  
  
    /**
    * Fonction de création d'un bateau.
    * creer : id : Int x taille : Int x positions : {(lig : Int, col: Int)} -> BateauProtocol
    * 1 < taille < 4 
    * positions : ensemble des coordonées initiales du bateau.
    * lig et col sont inclues dans les emplacements possibles du jeu. 0 <= lig, col <= tailleDuJeu 
    **/
    init(id: Int, taille: Int, positions: [(lig: Int, col: Int)]) 
  
    /** 
    * Touche une position non touché du bateau et met à jour celui-ci.
    * touchePosition : BateauProtocol x pos : (lig : Int, col: Int) -> BateauProtocol
    * pos est inclu dans les positions restantes (non touchées) du bateau.
    * lig et col sont inclues dans les emplacements possibles du jeu. 0 <= lig, col <= tailleDuJeu 
    **/
    mutating func touchePosition(pos :(lig:Int, col:Int)) -> Self 
                                        
    /** 
    * Retourne vrai si la position du bateau non touché est à cette position.
    * positionTouchee : BateauProtocol x pos : (lig : Int, col: Int)
    * pos est inclu dans les positions restantes (non touchées) du bateau.
    * lig et col inclues dans les emplacements possibles du jeu. 0 <= lig, col <= tailleDuJeu 
    **/
    func positionTouchee(pos :(lig:Int, col:Int)) -> Bool
  }
  
  struct Bateau : BateauProtocol{
    public private(set) var id : Int
    public private(set) var taille : Int
    public var estcoule : Bool{
        return nbPositionsRestantes == 0
      }
    public private(set) var positionsBat : [(lig:Int, col:Int)] 
    internal var positionsTouche : [(lig:Int, col:Int)?] 
    internal var positionsRestantes : [(lig: Int, col: Int)?] 
    private var nbPositionsTouche : Int
    private var nbPositionsRestantes : Int
  
    public init(id: Int, taille: Int, positions: [(lig: Int, col: Int)]){
      self.id = id
      self.taille = taille
      self.positionsBat = positions
      self.positionsTouche = [(lig: Int, col: Int)?](repeating: nil,  count: taille)
      self.positionsRestantes = positions
      self.nbPositionsTouche = 0
      self.nbPositionsRestantes = self.taille
    }
  
    mutating public func touchePosition(pos :(lig:Int, col:Int)) -> Self {
      for i in 0..<self.positionsRestantes.count {
        if let posNotNil = self.positionsRestantes[i] {
          if pos == posNotNil {
            self.positionsRestantes[i] = nil
            self.positionsTouche[self.taille - self.nbPositionsRestantes] = pos
            self.nbPositionsRestantes -= 1
            self.nbPositionsTouche += 1
          }
        }
      }
      return self
    }
  
    public func positionTouchee(pos :(lig:Int, col:Int)) -> Bool{
      var retour : Bool = false
      for position in self.positionsRestantes {
        // N'est pas null
        if let posNotNil = position{
          // Vérifie la condition
          if posNotNil == pos{
            retour = true
          }
        }
      }
      return retour
    }
  }
  
  protocol JeuProtocol{
    /**
    * La taille initiale du jeu.
    * taille : JeuProtocol -> (lig:Int, col:Int)
    * lig, col >= 5
    **/
    var taille : (lig:Int, col:Int) {get}
    /**
    * Le nombre de bateaux restants.
    * nbBatRestant : JeuProtocol -> Int
    **/
    var nbBatRestant : Int {get}
    /**
    * Le nombre de bateaux totals.
    * nbBatTotal : JeuProtocol -> Int
    **/
    var nbBatTotal : Int {get}
    /**
    * Le nombre de bateaux coulés.
    * nbBatCoule : JeuProtocol -> Int
    **/
    var nbBatCoule : Int {get} 
    /**
    * Tous les bateaux du jeu.
    * bateaux : JeuProtocol -> BateauProtocol
    **/
    var bateaux : [BateauProtocol] {get}
    /**
    * Tous les bateaux coulés du jeu.
    * batCoules : JeuProtocol -> {BateauProtocol|Vide}
    **/
    var batCoules : [BateauProtocol?] {get}
    /**
    * Etat du jeu.
    * gagne : JeuProtocol -> Bool
    * Vrai si la partie est gagné.
    * nbBatCoule == nbBatTotal ou nbBatRestant == 0
    **/
    var gagne : Bool {get}
    /**
    * Toutes les positions occupées par des bateaux
    * posOqp : JeuProtocol -> {(lig:Int, col:Int)|Vide}
    **/
    var posOqp : [(lig:Int, col:Int)?] {get}
  
    /**
    * Fonction de création d'un jeu.
    * jouer : (lig : Int, col: Int) -> JeuProtocol
    * Crée le jeu et implémente celui-ci avec 5 bateaux.
    * lig, col >= 5
    **/
    init(lig: Int, col:Int)
  
    /**
    * Vérifie s'il y a une position non touché d'un bateau sur la ligne.
    * oqpLigne : JeuProtocol x Int -> Bool 
    * lig est inclue dans les emplacements possibles du jeu. 0 <= lig <= tailleDuJeu 
    * Renvoie vrai s'il y a un bateau sur la ligne.
    **/
    func oqpLigne(lig: Int) -> Bool 
  
    /**
    * Vérifie s'il y a une position non touché d'un bateau sur la colonne.
    * oqpColonne : JeuProtocol x Int -> Bool
    * col est inclue dans les emplacements possibles du jeu. 0 <= col <= tailleDuJeu 
    * Renvoie vrai s'il y a un bateau sur la colonne.
    **/
    func oqpColonne(col: Int) -> Bool 
  
    /**
    * Vérifie s'il y a une position non touché d'un bateau sur la position
    * oqpPosition : JeuProtocol x pos : (lig : Int, col: Int) -> Bool
    * pos : un couple de lig, col 
    * lig et col sont inclues dans les emplacements possibles du jeu. 0 <= lig, col <= tailleDuJeu 
    * Renvoie vrai s'il y a un bateau sur la position
    **/
    func oqpPosition(pos : (lig : Int, col: Int)) -> Bool
  
    /**
    * Tire sur la position.
    * tirer : JeuProtocol x pos : (lig : Int, col: Int) -> JeuProtocol x ResTir
    * pos : un couple de lig, col 
    * lig et col sont inclues dans les emplacements possibles du jeu. 0 <= lig, col <= tailleDuJeu 
    * Renvoie si un bateau est coulé, touché, en vue ou a l'eau.
    **/
    mutating func tirer(pos: (lig: Int, col: Int)) -> ResTir
  
    /**
    * bateau : JeuProtocol x pos: (lig : Int, col : Int) -> BateauProtocol|Vide
    * Revoie un bateau ou non qui se trouve sur la position donnée en paramettre
    * pos : un couple de lig, col 
    * lig et col sont inclues dans les emplacements possibles du jeu. 0 <= lig, col <= tailleDuJeu 
    **/
    func bateau(pos:(lig:Int, col:Int)) -> BateauProtocol?
  }
  
  
  struct Jeu : JeuProtocol{
    public private(set) var taille : (lig:Int, col:Int)
    public private(set) var nbBatRestant : Int
    public var nbBatTotal : Int {
      return bateaux.count
    }
    public var nbBatCoule : Int {
      return nbBatTotal - nbBatRestant
    }
    public private(set) var bateaux : [BateauProtocol]
    public private(set) var batCoules : [BateauProtocol?]
    private var privBatCoules : [Int?]
    public var gagne : Bool{
      return nbBatCoule == nbBatTotal
    }
    public private(set) var posOqp : [(lig:Int, col:Int)?]
    
  
  
    public init(lig: Int, col:Int){
      self.taille = (lig, col)
      let Bateau1 : BateauProtocol = Bateau(id: 1, taille: 3, positions: [(lig: 0, col: 0), (lig: 1, col: 0) , (lig: 2, col: 0)])
      let Bateau2 : BateauProtocol = Bateau(id: 2, taille: 3, positions: [(lig: 3, col: 0), (lig: 3, col: 1) , (lig: 3, col: 2)])
      let Bateau3 : BateauProtocol = Bateau(id: 3, taille: 2, positions: [(lig: 2, col: 2), (lig: 2, col: 1)])
      let Bateau4 : BateauProtocol = Bateau(id: 4, taille: 1, positions: [(lig: 4, col: 4)])
      let Bateau5 : BateauProtocol = Bateau(id: 5, taille: 4, positions: [(lig: 0, col: 4), (lig: 1, col: 4), (lig: 2, col: 4) , (lig: 3, col: 4)])
      self.bateaux = [Bateau1, Bateau2, Bateau3, Bateau4, Bateau5]
      self.nbBatRestant = bateaux.count
      self.privBatCoules = [Int?](repeating: nil,  count: 5)
      var index = 0
      self.posOqp = [(lig: Int, col: Int)?](repeating: nil,  count: 13)
      for bat in bateaux{
        for pos in bat.positionsBat{
          self.posOqp[index] = pos
          index += 1
        }
      }
      self.batCoules = [BateauProtocol?](repeating: nil,  count: 5)
    }
  
    public func oqpLigne(lig: Int) -> Bool {
      var res : Bool = false
      for pos in self.posOqp{
        if let oqlLig = pos?.lig{
          if oqlLig == lig {
            res = true
          } 
        }
      }
      return res
    }
  
    public func oqpColonne(col: Int) -> Bool {
      var res : Bool = false
      for pos in self.posOqp{
        if let oqlCol = pos?.col{
          if oqlCol == col {
            res = true
          } 
        }
      }
      return res
    }
  
    public func oqpPosition(pos : (lig : Int, col: Int)) -> Bool {
      var res : Bool = false
      for posi in self.posOqp{
        if let oqlLig = posi?.lig , let oqlCol = posi?.col{
          if oqlLig == pos.lig , oqlCol == pos.col {
            res = true
          }
        }
      }
      return res
    }
  
    mutating private func tirerBis(pos : (lig: Int, col: Int)) -> ResTir{
      // je cherche s'il y a un bateau sur la position
      var res : ResTir = ResTir.aleau
      if var bat = bateau(pos: pos){ 
        // est-ce que la position n'a pas été touché
        if bat.positionTouchee(pos: pos) == true{
          // si oui
          //on touche la position
          bat.touchePosition(pos: pos)
          // on vérifie si le bateau est touché ou coulé
          if bat.estcoule == true{
            res = ResTir.coule
            self.nbBatRestant -= 1
          }
          else{
            res = ResTir.touche
          }
        }
        // si non (potision bateau déjà touchée)
        else {
          res = estEnvue(pos : pos)
        }
      }
      // sinon (pas de bateau sur la position)
      else {
        res = estEnvue(pos: pos)
      }
      return res
    }
  
    private func estEnvue(pos : (lig : Int, col : Int)) -> ResTir{
      var res : ResTir = ResTir.aleau
      // on vérifie si bateau sur ligne ou colonne
      if oqpLigne(lig: pos.lig) || oqpColonne(col: pos.col){
        // si oui -> envue
        res = ResTir.envue
      }
      return res
    }
  
    mutating public func tirer(pos: (lig: Int, col: Int)) -> ResTir{
      // je cherche s'il y a un bateau sur la position
      var res : ResTir = ResTir.aleau
      if let id = idxBateau(pos: pos){ 
        // est-ce que la position n'a pas été touché
        if self.bateaux[id].positionTouchee(pos : pos) == true{
          // si oui
          //on touche la position
          self.bateaux[id].touchePosition(pos : pos)
          // on vérifie si le bateau est touché ou coulé
          if self.bateaux[id].estcoule == true{
            res = ResTir.coule
            self.batCoules[nbBatCoule] = self.bateaux[id]
            self.nbBatRestant -= 1
          }
          else{
            res = ResTir.touche
          }
        }
        // si non (potision bateau déjà touchée)
        else {
          res = estEnvue(pos: pos)
        }
      }
      // sinon (pas de bateau sur la position)
      else {
        res = estEnvue(pos: pos)
      }
      return res
    }
    
    public func bateau(pos:(lig:Int, col:Int)) -> BateauProtocol? {
      var res : BateauProtocol? = nil
      for bat in bateaux {
        for posB in bat.positionsRestantes{
          if let pLig = posB?.lig , let pCol = posB?.col{
            if pLig == pos.lig , pCol == pos.col{
              res = bat
            }
          }
        }
      }
      return res
    }
    
    private func idxBateau(pos:(lig:Int, col:Int))-> Int?{
      var i : Int = 0
      while i < bateaux.count{
        var j : Int = 0
        while j < bateaux[i].positionsBat.count{
          if bateaux[i].positionsBat[j].lig == pos.lig && bateaux[i].positionsBat[j].col == pos.col{
            return i
          }
          j += 1
        }
        i += 1
      }
      return nil
    }
  }
  
  enum ResTir : String {
    case envue = "En vue !"
    case touche = "Touché !"
    case coule = "Coulé !"
    case aleau = "A l'eau !"
  }
  
  func inputInt(message: String, max : Int = 9) -> Int{
      var reponse : String? = nil
      var rep : Int? = nil
      repeat{
          print(message)
          reponse = readLine()
          if let reponse = reponse{
              rep = Int(reponse)
          }
      } while (reponse == nil) || (rep == nil) || ((rep! < 0) || (rep! > max))
      return rep!
  }
  
  var jeu = Jeu(lig:20, col: 20)
  while !jeu.gagne{
    let lig = inputInt(message: "donnez une ligne comprise entre 0 et \(jeu.taille.lig - 1)")
    let col = inputInt(message: "donnez une colonne comprise entre 0 et \(jeu.taille.col - 1)")
    let res = jeu.tirer(pos: (lig: lig,col: col))
    switch res{
      case .touche: print("Touché !")
      case .coule: 
          print("Coulé !")
          print("Il reste encore \(jeu.nbBatRestant) à couler")
      case .envue: print("loupé mais il y a un bateau sur la même ligne ou la même colonne")
      case .aleau: print("À l'eau : pas même un bateau en vue")
    }
  }
  