
protocol protocolBateau {
 var id : Int {get} 
  // identifiant du bateau
  // id: protocolBateau -> Int
 var taille : Int {get} 
  // 0<=taille<=5 taille du bateau
  // taille: protocolBateau -> Int
 var estCoule : Bool {get} 
  // le bateau est-il coulé ?
  // estCoule: protocolBateau -> Bool
 var PosBateau : [(lig:Int,col:Int)] {get}
  // liste des positions des bateaux
  // 0<col,lig<=jeu.taille
  // PosBateau: protocolBateau -> (lig: Int, col: Int)
 var PosTouchees : [(lig:Int,col:Int)?] {get set}
  // liste des positions que l'on a touchee
  // PosTouchees: protocolBateau -> ((lig: Int, col: int)||vide)
 var PosRestantes: [(lig:Int,col:Int)?] {get set}
  // liste des positions restantes
  // PosRestantes: protocolBateau -> ((lig: Int, col: int)||vide)
  
init(id: Int, taille: Int, pos: [(lig: Int, col: Int)])
  // fonction pour creer les bateaux avec son identifiant, sa taille et sa position
  // bateau: id x taille x pos -> protocolbateau
mutating func Toucheposition(pos :(lig:Int, col:Int)) -> Self
  // touche une position qui n'a pas ete touchee d'un bateau
  // Toucheposition: protocolBateau x pos -> protocolBateau
func PosTouchees(pos :(lig:Int, col:Int)) -> Bool
  // fonction qui renvoie vrai si la position correspond a celle d'un bateau non-touche
  // PosTouchees: protocolBateau x pos -> Bool
}

struct bateau : protocolBateau {
 public private(set) var id : Int 
 public private(set) var taille : Int 
 public var estCoule : Bool {
   return nbposres == 0 
 }
 public private(set) var PosBateau : [(lig:Int,col:Int)] 
 internal var PosTouchees : [(lig:Int,col:Int)?] 
 internal var PosRestantes : [(lig:Int,col:Int)?]
 private var nbposres : Int 
 private var nbpostouchee : Int 
  
public init(id:Int,taille:Int,pos:[(lig:Int,col:Int)]){
 self.id=id
 self.taille=taille
 self.PosBateau=pos
 self.PosRestantes=pos
 self.PosTouchees=[(lig:Int,col:Int)?](repeating: nil,count: taille)
 self.nbposres = self.taille
 self.nbpostouchee = 0
}
 
public func PosTouchees (pos:(lig:Int,col:Int))->Bool{
 var retour = false
 for PosRest in self.PosRestantes {
   if let PosRestNnil = PosRest{
     if PosRestNnil == pos{
       retour = true
     }
   }
 }
 return retour 
 }

mutating public func Toucheposition(pos:(lig:Int,col:Int))->Self{
  for i in 0..<self.PosRestantes.count {
    if let posNnil = self.PosRestantes[i] {
    if pos == posNnil {
      self.PosRestantes[i] = nil
      self.PosTouchees[self.taille - self.nbposres] = pos
      self.nbposres -= 1
      self.nbpostouchee += 1
        }
      }
    }
    return self
  }  
}

protocol protocoljeu {
var taille : (lig : Int, col : Int) {get}
  // la taille du jeu
  // taille: protocoljeu -> (lig: Int, col: Int)
var nbBatRest : Int {get}
  // le nombre de bateaux restants
  // nbBatRest: protocoljeu -> Int
var nbBatTotal : Int {get}
  // le nombre total de bateaux
  // nbBatTotal: protocoljeu -> Int
var nbBatCoulee : Int {get}
  // le nombre de bateaux coules
  // nbBatCoulee: protocoljeu -> Int
var Bateaux : [bateau] {get}
  // les bateaux
  // Bateaux: protocoljeu -> bateau
var batCoulee : [bateau?] {get}
  // les bateaux coules
  // batCoulee: protocoljeu -> (bateau||vide)
var gagne : Bool {get}
  // vrai si le jeu est fini (partie gagnee)
  // gagne: protocoljeu -> Bool
var posOQP : [(lig : Int, col : Int)?] {get}
  // liste des positions occupees
  // posOQP: protocoljeu -> ((lig: Int, col: Int)||vide)
init(lig:Int,col:Int)
  // fonction pour creer le jeu
func oqpligne(lig:Int) -> Bool
  // fonction qui renvoie vrai si il y a la position d'un bateau non-touche sur la ligne
  // oqpligne: protocoljeu x Int -> Bool
func oqpcol(col:Int) -> Bool
    // fonction qui renvoie vrai si il y a la position d'un bateau non-touche sur la colonne
    // oqpcol: protocoljeu x Int -> Bool
func positionOQP(pos:(lig:Int,col:Int)) -> Bool
    // fonction qui renvoie vrai si il y a la position d'un bateau non-touche sur la position
    // positionOQP: protocoljeu x pos -> Bool
mutating func tirer(pos: (lig:Int,col:Int)) -> ResTir
  // fonction qui tire sur un bateau, renvoie si un bateau est touche ou coule ou en vue ou si le tire est a l'eau
  // tirer: protocoljeu x pos -> protocoljeu x ResTir
func Bateau(pos:(lig:Int,col:Int)) -> protocolBateau?
  // renvoie un bateau si il se trouve sur la position sinon ne renvoie rien
  // Bateau: protocoljeu x pos -> (protocolBateau||vide)
}

struct Jeu : protocoljeu{
 public private(set) var taille : (lig : Int, col : Int)
 public private(set) var nbBatRest : Int
  
 public var nbBatTotal : Int {
   return Bateaux.count
 }
public var nbBatCoulee : Int {
  return nbBatTotal - nbBatRest
}
 public private(set) var Bateaux : [bateau]
 public private(set) var batCoulee : [bateau?]
 public var gagne : Bool {
   return nbBatCoulee == nbBatTotal
 }
 public private(set) var posOQP : [(lig : Int, col : Int)?]
private var privbatcoulee : Int



public init (lig : Int, col : Int){
  self.taille = (lig,col)
  let Bateau1  = bateau (id: 1, taille: 3, pos: [(lig: 0, col: 0), (lig: 1, col: 0) , (lig: 2, col: 0)])
  let Bateau2  = bateau(id: 2, taille: 3, pos: [(lig: 3, col: 0), (lig: 3, col: 1) , (lig: 3, col: 2)])
  let Bateau3 = bateau(id: 3, taille: 2, pos: [(lig: 2, col: 2), (lig: 2, col: 1)])
  let Bateau4 = bateau(id: 4, taille: 1, pos: [(lig: 4, col: 4)])
  let Bateau5 = bateau(id: 5, taille: 4, pos: [(lig: 0, col: 4), (lig: 1, col: 4), (lig: 2, col: 4) , (lig: 3, col: 4)])
  self.Bateaux = [Bateau1,Bateau2,Bateau3,Bateau4,Bateau5 ]
  self.nbBatRest = Bateaux.count
  self.privbatcoulee = Bateaux.count
  var index = 0
  self.posOQP = [(lig:Int, col:Int)?]( repeating:nil , count :13)
  for bat in Bateaux {
    for pos in bat.PosBateau {
      self.posOQP[index] = pos
          index += 1
    }
  }
self.batCoulee = [bateau?](repeating:nil, count:5)
 } 
 
public func oqpligne(lig : Int)-> Bool{
  var res : Bool = false
  for pos in self.posOQP{
    if let oqlLig = pos?.lig{
      if oqlLig == lig {
        res = true
      }
    }
  }
  return res 
}
 
 
public func oqpcol(col : Int)-> Bool{
  var res = false
  for pos in self.posOQP{
    if let oqlCol = pos?.col{
      if oqlCol == col {
        res = true
      }
    }
  }
  return res 
 }

public func positionOQP(pos: (lig : Int, col : Int)) -> Bool{
  var res : Bool = false
  for posb in self.posOQP{
    if let oqlLig = posb?.lig , let oqlCol = posb?.col{
      if oqlLig == pos.lig , oqlCol == pos.col {
        res = true
      }
    }
  }
  return res
}
 
mutating public func tirer(pos: (lig: Int, col: Int)) -> ResTir{
  var res : ResTir = ResTir.aleau
  if let id = idBateau(pos: pos){ 
    if self.Bateaux[id].PosTouchees(pos: pos) == true{
      self.Bateaux[id].Toucheposition(pos: pos)
      if self.Bateaux[id].estCoule == true{
        res = ResTir.coule
        self.batCoulee[nbBatCoulee] = self.Bateaux[id]
        self.nbBatRest -= 1
      }
      else{
        res = ResTir.touche
      }
    }
    else {
      res = estEnvue(pos: pos)
    }
  }
  else {
    res = estEnvue(pos: pos)
  }
  return res
}
  



private func estEnvue(pos : (lig : Int, col : Int)) -> ResTir{
  var res : ResTir = ResTir.aleau
  if oqpligne(lig: pos.lig) || oqpcol(col: pos.col){
    res = ResTir.envue
  }
  return res
}

public func Bateau(pos:(lig:Int, col:Int)) -> protocolBateau? {
  var res : protocolBateau? = nil
  for bat in Bateaux {
    for posb in bat.PosRestantes{
      if let plig = posb?.lig , let pcol = posb?.col{
        if plig == pos.lig , pcol == pos.col{
          res = bat
        }
      }
    }
  }
  return res
}

private func idBateau(pos:(lig:Int, col:Int))-> Int?{
  var i : Int = 0
  while i < Bateaux.count{  
    var j : Int = 0
    while j < Bateaux[i].PosBateau.count{
      if Bateaux[i].PosBateau[j].lig == pos.lig &&           Bateaux[i].PosBateau[j].col == pos.col{
        return i
      }
    j += 1
    }
    i += 1
  }
  return nil
}
}

enum ResTir : String{
 case envue = "En vue!"
 case aleau = "A l'eau!"
 case touche = "Touché!"
 case coule = "Coulé!"
 }

func input(message: String, max : Int = 9) -> Int{
  var reponse : String? = nil
  var rep : Int? = nil
  repeat{
    print(message)
    reponse = readLine()
    if let reponse = reponse{
      rep = Int(reponse)
    }
  } 
  while (reponse == nil) || (rep == nil) || ((rep! < 0) || (rep! > max))
  return rep!
}

var jeu = Jeu(lig:20, col: 20)
  while !jeu.gagne{
    let lig = input(message: "donnez une ligne comprise entre 0 et \(jeu.taille.lig - 1)")
    let col = input(message: "donnez une colonne comprise entre 0 et \(jeu.taille.col - 1)")
    let res = jeu.tirer(pos: (lig: lig,col: col))
    switch res{
      case .touche: print("Touché !")
      case .coule: 
          print("Coulé !")
          print("Il reste encore \(jeu.nbBatRest) bateau(x) à couler")
      case .envue: print("loupé mais il y a un bateau sur la même ligne ou la même colonne")
      case .aleau: print("À l'eau : pas même un bateau en vue")
    }
  }